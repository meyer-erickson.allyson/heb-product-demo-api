# HEBProductsDemoAPI

CRUD application that allows the user to interact with a database of Products.    
Built using Spring Boot and Gradle.    
Must be run on port 8080 to work with frontend Angular application.    

## Roadmap

- [ ] Additional unit tests
- [ ] Set up reverse proxy 
- [ ] Return 204 error on product not found, instead of 400
- [ ] Possibly change entity to record
