package com.example.productsapi.repository;

import com.example.productsapi.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

//uses the Product class and Long type for primary key
//automatically has access to common CRUD operations
public interface ProductRepository extends JpaRepository<Product, Long> {

}
