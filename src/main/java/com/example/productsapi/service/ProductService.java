package com.example.productsapi.service;

import com.example.productsapi.entities.Product;
import com.example.productsapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    /** GET all products */
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    /** GET product by ID */
    public Product getProductById(long id) {
        return productRepository.findById(id).orElse(null);
    }

    /** POST: add product */
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    /** PUT: update product */
    public Product updateProduct(Long id, Product updatedProduct) {
        Product existingProduct = productRepository.findById(id).orElse(null);
        if(existingProduct != null) {
            existingProduct.setUpc(updatedProduct.getUpc());
            existingProduct.setName(updatedProduct.getName());
            existingProduct.setDescription(updatedProduct.getDescription());
        }
        return productRepository.save(existingProduct);
    }

    /** DELETE product by ID */
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }
}
