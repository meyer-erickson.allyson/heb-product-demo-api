package com.example.productsapi.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = {"id"})
@Data
@Entity
@Table(name = "products")
public class Product {
    @Id
    @SequenceGenerator(name = "product_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_id_seq")
    private Long id;

    @NotBlank(message = "UPC is mandatory")
    @Digits(integer = 12, fraction = 0)
    private String upc;

    @NotBlank(message = "name is mandatory")
    @Size(min = 3, max = 20)
    private String name;

    @NotBlank(message = "description is mandatory")
    @Size(min = 3, max = 255)
    private String description;

    public Product(Long id, String upc, String name, String description) {
        this.id = id;
        this.upc = upc;
        this.name = name;
        this.description = description;
    }

    public Product() { }
}
