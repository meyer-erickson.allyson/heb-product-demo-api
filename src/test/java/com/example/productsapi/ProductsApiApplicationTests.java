package com.example.productsapi;

import com.example.productsapi.entities.Product;
import com.example.productsapi.repository.ProductRepository;
import com.example.productsapi.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@RequiredArgsConstructor
class ProductsApiApplicationTests {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    private final Product testProduct = new Product(
        1L,
        "999999999999",
        "testProduct",
        "testProduct"
    );


    @Test
    void contextLoads() {
    }

    /** getAllProducts() tests */
    @Test
    public void getAllProducts() {
        List<Product> expectedProducts = productRepository.findAll();
        List<Product> actualProducts = productService.getAllProducts();
        assertEquals(expectedProducts, actualProducts);
    }

    /** getProductById() tests */
    @Test
    public void getProductById() {
        Product expectedProduct = productRepository.findById(1L).orElse(null);
        Product actualProduct = productService.getProductById(1L);
        assertEquals(expectedProduct, actualProduct);
    }

    @Test
    public void getProductById_whenProductNotFound_shouldReturnNull() {
        Product expectedProduct = productRepository.findById(-1L).orElse(null);
        Product actualProduct = productService.getProductById(-1L);
        assertEquals(expectedProduct, actualProduct);
    }

    /** addProduct() tests */
    @Test
    public void addProduct() {
        Product actualProduct = productService.addProduct(testProduct);
        assertEquals(testProduct.getName(), actualProduct.getName());
    }

    /** updateProduct() tests */
    @Test
    public void updateProduct() {
        Product actualProduct = productService.updateProduct(1L, testProduct);
        assertEquals(testProduct.getName(), actualProduct.getName());
    }

    /** deleteProductById() tests */
    @Test
    public void deleteProductById() {
        Product savedProduct = productRepository.save(testProduct);
        productService.deleteProductById(savedProduct.getId());
        Product foundProduct = productRepository.findById(savedProduct.getId()).orElse(null);
        assertNull(foundProduct);
    }
}
